# jualan

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

# how to deploy to heroku

https://nuxtjs.org/faq/heroku-deployment

First, we want our application to listen on the host 0.0.0.0 and run in production mode:

```
$ heroku config:set HOST=0.0.0.0
$ heroku config:set NODE_ENV=production
```

Add Procfile

Heroku uses a Procfile (name the file Procfile with no file extension) that specifies the commands that are executed by the apps dynos. To start the Procfile will be very simple, and needs to contain the following line:

```
web: nuxt start
```

Finally, we can push the app on Heroku with:

```
$ git push heroku master
```

To deploy a non-master branch to Heroku use:

```
git push heroku develop:master
```
