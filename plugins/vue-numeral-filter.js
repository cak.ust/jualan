import Vue from "vue";
import numeral from "numeral";

Vue.use(numeral);
Vue.filter("formatNumber", function(value) {
  return numeral(value).format("0,0"); // displaying other groupings/separators is possible, look at the docs
});
