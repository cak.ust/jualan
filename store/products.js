export const state = () => ({
  products: [],
  selectedProduct: {},
  selectedImage: "",
  productVariants: [],
  selectedColor: {},
  selectedSize: {},
  selectedVariant: {},
  searchProduct: "",
  sortProduct: "name,asc",
  productCart: []
});

export const actions = {
  async fetch({ commit, state }) {
    commit("SET_LOADING", true, { root: true });
    const response = await this.$axios.$get("/products-all", {
      params: {
        search: state.searchProduct,
        sort: state.sortProduct
      }
    });
    commit("SET_PRODUCTS", response);
    commit("SET_LOADING", false, { root: true });
  },

  async findById({ commit }, id) {
    const response = await this.$axios.$get(`/products/${id}`);
    commit("SET_SELECTED_PRODUCT", response);
    if (response.images && response.images.length > 0) {
      commit("SET_SELECTED_IMAGE", response.images[0].image);
    }
  },

  async findProductVariants({ commit }, params) {
    const productVariant = await this.$axios.$get(
      `/products/variants/${params.productId}/${params.colorId}/${params.sizeId}`
    );
    commit("SET_PRODUCT_VARIANT", productVariant ? productVariant : {});
  },

  addProductToCart({ commit }, payload) {
    commit("ADD_PRODUCT_CART", payload);
  }
};

export const mutations = {
  SET_PRODUCTS(state, products) {
    state.products = products;
  },
  SET_SELECTED_PRODUCT(state, product) {
    state.selectedProduct = product;
  },
  SET_PRODUCT_VARIANT(state, variant) {
    state.selectedVariant = variant;
  },
  SET_SELECTED_COLOR(state, color) {
    state.selectedColor = color;

    if (
      color &&
      typeof color !== "undefined" &&
      color.image &&
      typeof color.image !== "undefined" &&
      color.image != ""
    ) {
      state.selectedImage = color.image;
    }
  },
  SET_SELECTED_SIZE(state, size) {
    state.selectedSize = size;
  },
  SET_SELECTED_IMAGE(state, image) {
    state.selectedImage = image;
  },
  SET_SEARCH_PRODUCT(state, search) {
    state.searchProduct = search;
  },
  ADD_PRODUCT_CART(state, product) {
    state.productCart.push(product);
  },
  SET_SORTING_PRODUCT(state, sort) {
    state.sortProduct = sort;
  }
};
